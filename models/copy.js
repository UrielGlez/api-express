module.exports = (sequelize, type) => {
  const Copy = sequelize.define("copy", {
    id: { type: type.INTEGER, primaryKey: true, autoIncrement: true },
    number: { type: type.INTEGER, notNull: true },
    format: { type: type.ENUM, values: ['16:9', '1:1', '4:3'] },
    status: { type: type.ENUM, values: ['x', 'y', 'z'] }
  });

  return Copy;
};
