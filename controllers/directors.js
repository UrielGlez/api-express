const express = require("express");
const { Director } = require("../db");

function list(req, res, next) {
  Director.findAll()
    .then((directores) =>
      res.status(200).json({
        message: "lista de Directores enviada correctamente",
        objs: directores,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "Error: lista de Directores no enviada",
        objs: {},
      })
    );
}

function index(req, res, next) {
  let id = req.params.id;

  Director.findByPk(id)
    .then((director) =>
      res.status(200).json({
        message: "Director encontrado",
        objs: director,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "Error: Director no encontrado",
        objs: {},
      })
    );
}

function create(req, res, next) {
  let director = new Object();
  director.name = req.body.name;
  director.lastName = req.body.lastName;

  Director.create(director)
    .then((director) =>
      res.status(200).json({
        message: "Director creado correctamente",
        objs: director,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "No se pudo crear el director",
        objs: {},
      })
    );
}

function update(req, res, next) {
  let id = req.body.id;
  let director = new Object();

  director.name = req.body.name;
  director.lastName = req.body.lastName;

  Director.update(director, {
    where: {
      id: id,
    },
  })
    .then(() =>
      res.status(200).json({
        message: "Director actualizado correctamente",
        objs: director,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "No se pudo actualizar el director",
        objs: {},
      })
    );
}

function destroy(req, res, next) {
  let id = req.body.id;
  Director.destroy({
    where: {
      id: id,
    },
  })
    .then(() =>
      res.status(200).json({
        message: "Director eliminado",
        objs: {
          id: parseInt(id),
        },
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "No se pudo eliminar al Director",
        objs: {},
      })
    );
}

module.exports = {
  list,
  index,
  create,
  update,
  destroy,
};
