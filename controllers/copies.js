const express = require("express");
const { Copy } = require("../db");

function list(req, res, next) {
  Copy.findAll()
    .then((copies) =>
      res.status(200).json({
        message: "lista de Copies enviada correctamente",
        objs: copies,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "Error: lista de Copies no enviada",
        objs: {},
      })
    );
}

function index(req, res, next) {
  let id = req.params.id;

  Copy.findByPk(id)
    .then((copy) =>
      res.status(200).json({
        message: "Copy encontrado",
        objs: copy,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "Error: Copy no encontrado",
        objs: {},
      })
    );
}

function create(req, res, next) {
  let copy = new Object();
  copy.number = req.body.number;
  copy.movie_id = req.body.movie_id;
  copy.format = req.body.format;
  copy.status = req.body.status;

  Copy.create(copy).then((copy) =>
    res.status(200).json({
      message: "Copy creada correctamente",
      objs: copy,
    })
  );
}

function update(req, res, next) {
  let id = req.body.id;
  let copy = new Object();

  copy.number = req.body.number;
  copy.movie_id = req.body.movie_id;
  copy.format = req.body.format;
  copy.status = req.body.status;

  Copy.update(copy, {
    where: {
      id: id,
    },
  })
    .then(() =>
      res.status(200).json({
        message: "Copy actualizado correctamente",
        objs: copy,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "No se pudo actualizar el copy",
        objs: {},
      })
    );
}

function destroy(req, res, next) {
  let id = req.body.id;
  Copy.destroy({
    where: {
      id: id,
    },
  })
    .then(() =>
      res.status(200).json({
        message: "Copy eliminado",
        objs: {
          id: parseInt(id),
        },
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "No se pudo eliminar al copy",
        objs: {},
      })
    );
}

module.exports = {
  list,
  index,
  create,
  update,
  destroy,
};
