const express = require("express");
const { Booking } = require("../db");

function list(req, res, next) {
  Booking.findAll()
    .then((bookings) =>
      res.status(200).json({
        message: "lista de bookings enviada correctamente",
        objs: bookings,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "Error: lista de bookings no enviada",
        objs: {},
      })
    );
}

function index(req, res, next) {
  let id = req.params.id;

  Booking.findByPk(id)
    .then((booking) =>
      res.status(200).json({
        message: "Booking encontrado",
        objs: booking,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "Error: Booking no encontrado",
        objs: {},
      })
    );
}

function create(req, res, next) {
  let booking = new Object();
  booking.date = req.body.date;
  booking.member_id = req.body.member_id;
  booking.copy_id = req.body.copy_id;

  Booking.create(booking).then((booking) =>
    res.status(200).json({
      message: "Booking creado correctamente",
      objs: booking,
    })
  );
}

function update(req, res, next) {
  let id = req.body.id;
  let booking = new Object();

  booking.date = req.body.date;
  booking.member_id = req.body.member_id;
  booking.copy_id = req.body.copy_id;

  Booking.update(booking, {
    where: {
      id: id,
    },
  })
    .then(() =>
      res.status(200).json({
        message: "Booking actualizado correctamente",
        objs: booking,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "No se pudo actualizar el booking",
        objs: {},
      })
    );
}

function destroy(req, res, next) {
  let id = req.body.id;
  Booking.destroy({
    where: {
      id: id,
    },
  })
    .then(() =>
      res.status(200).json({
        message: "Booking eliminado",
        objs: {
          id: parseInt(id),
        },
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "No se pudo eliminar al booking",
        objs: {},
      })
    );
}

module.exports = {
  list,
  index,
  create,
  update,
  destroy,
};
