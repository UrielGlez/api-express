const express = require("express");
const { Member } = require("../db");

function list(req, res, next) {
  Member.findAll()
    .then((members) =>
      res.status(200).json({
        message: "lista de Members enviada correctamente",
        objs: members,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "Error: lista de Members no enviada",
        objs: {},
      })
    );
}

function index(req, res, next) {
  let id = req.params.id;

  Member.findByPk(id)
    .then((members) =>
      res.status(200).json({
        message: "Members encontrado",
        objs: members,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "Error: Members no encontrado",
        objs: {},
      })
    );
}

function create(req, res, next) {
  let member = new Object();
  member.name = req.body.name;
  member.lastName = req.body.lastName;
  member.address = req.body.address;
  member.phone = req.body.phone;
  member.status = req.body.status;

  Member.create(member).then((member) =>
    res.status(200).json({
      message: "Member creado correctamente",
      objs: member,
    })
  );
}

function update(req, res, next) {
  let id = req.body.id;
  let member = new Object();

  member.name = req.body.name;
  member.lastName = req.body.lastName;
  member.address = req.body.address;
  member.phone = req.body.phone;
  member.status = req.body.status;

  Member.update(member, {
    where: {
      id: id,
    },
  })
    .then(() =>
      res.status(200).json({
        message: "Member actualizado correctamente",
        objs: member,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "No se pudo actualizar el Member",
        objs: {},
      })
    );
}

function destroy(req, res, next) {
  let id = req.body.id;
  Member.destroy({
    where: {
      id: id,
    },
  })
    .then(() =>
      res.status(200).json({
        message: "Member eliminado",
        objs: {
          id: parseInt(id),
        },
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "No se pudo eliminar al Member",
        objs: {},
      })
    );
}

module.exports = {
  list,
  index,
  create,
  update,
  destroy,
};
