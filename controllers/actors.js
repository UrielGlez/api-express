const express = require("express");
const { Actor } = require("../db");

function list(req, res, next) {
  Actor.findAll()
    .then((actors) =>
      res.status(200).json({
        message: "lista de Actores enviada correctamente",
        objs: actors,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "Error: lista de actores no enviada",
        objs: {},
      })
    );
}

function index(req, res, next) {
  let id = req.params.id;

  Actor.findByPk(id)
    .then((actor) =>
      res.status(200).json({
        message: "Actor encontrado",
        objs: actor,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "Error: Actor no encontrado",
        objs: {},
      })
    );
}

function create(req, res, next) {
  let actor = new Object();

  actor.name = req.body.name;
  actor.lastName = req.body.lastName;

  Actor.create(actor).then((actor) =>
    res.status(200).json({
      message: "Actor creado correctamente",
      objs: actor,
    })
  );
}

function update(req, res, next) {
  let id = req.body.id;
  let actor = new Object();

  actor.name = req.body.name;
  actor.lastName = req.body.lastName;

  Actor.update(actor, {
    where: {
      id: id,
    },
  })
    .then(() =>
      res.status(200).json({
        message: "Actor actualizado correctamente",
        objs: actor,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "No se pudo actualizar el actor",
        objs: {},
      })
    );
}

function destroy(req, res, next) {
  let id = req.body.id;
  Actor.destroy({
    where: {
      id: id,
    },
  })
    .then(() =>
      res.status(200).json({
        message: "Actor eliminado",
        objs: {
          id: parseInt(id),
        },
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "No se pudo eliminar al actor",
        objs: {},
      })
    );
}

module.exports = {
  list,
  index,
  create,
  update,
  destroy,
};
