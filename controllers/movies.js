const express = require("express");
const { Movie } = require("../db");

function list(req, res, next) {
  Movie.findAll()
    .then((movies) =>
      res.status(200).json({
        message: "lista de Movies enviada correctamente",
        objs: movies,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "Error: lista de Movies no enviada",
        objs: {},
      })
    );
}

function index(req, res, next) {
  let id = req.params.id;

  Movie.findByPk(id)
    .then((movie) =>
      res.status(200).json({
        message: "Movie encontrado",
        objs: movie,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "Error: Movie no encontrado",
        objs: {},
      })
    );
}

function create(req, res, next) {
  let movie = new Object();
  movie.title = req.body.title;
  movie.genre_id = req.body.genre_id;
  movie.director_id = req.body.director_id;
  
  Movie.create(movie).then((movie) =>
    res.status(200).json({
      message: "Movie creada correctamente",
      objs: movie
    })
  );
}

function update(req, res, next) {
  let id = req.body.id;
  let movie = new Object();

  movie.title = req.body.title;
  movie.genre_id = req.body.genre_id;
  movie.director_id = req.body.director_id;

  Movie.update(movie, {
    where: {
      id: id,
    },
  })
    .then(() =>
      res.status(200).json({
        message: "Movie actualizado correctamente",
        objs: movie,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "No se pudo actualizar el Movie",
        objs: {},
      })
    );
}

function destroy(req, res, next) {
  let id = req.body.id;
  Movie.destroy({
    where: {
      id: id,
    },
  })
    .then(() =>
      res.status(200).json({
        message: "Movie eliminada",
        objs: {
          id: parseInt(id),
        },
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "No se pudo eliminar la Movie",
        objs: {},
      })
    );
}

module.exports = {
  list,
  index,
  create,
  update,
  destroy,
};