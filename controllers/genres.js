const express = require("express");
const { Genre } = require("../db");

function list(req, res, next) {
  Genre.findAll()
    .then((genres) =>
      res.status(200).json({
        message: "lista de Genres enviada correctamente",
        objs: genres,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "Error: lista de genres no enviada",
        objs: {},
      })
    );
}

function index(req, res, next) {
  let id = req.params.id;

  Genre.findByPk(id)
    .then((genre) =>
      res.status(200).json({
        message: "Genre encontrado",
        objs: genre,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "Error: Genre no encontrado",
        objs: {},
      })
    );
}

function create(req, res, next) {
  let genre = new Object();
  genre.description = req.body.description;
  genre.status = req.body.status;
  
  Genre.create(genre).then((genre) =>
    res.status(200).json({
      message: "Genero creado correctamente",
      objs: genre
    })
  );
}

function update(req, res, next) {
  let id = req.body.id;
  let genre = new Object();

  genre.description = req.body.description;
  genre.status = req.body.status;

  Genre.update(genre, {
    where: {
      id: id,
    },
  })
    .then(() =>
      res.status(200).json({
        message: "genre actualizado correctamente",
        objs: genre,
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "No se pudo actualizar el genre",
        objs: {},
      })
    );
}

function destroy(req, res, next) {
  let id = req.body.id;
  Genre.destroy({
    where: {
      id: id,
    },
  })
    .then(() =>
      res.status(200).json({
        message: "Genre eliminado",
        objs: {
          id: parseInt(id),
        },
      })
    )
    .catch((error) =>
      res.status(500).json({
        message: "No se pudo eliminar al Genre",
        objs: {},
      })
    );
}

module.exports = {
  list,
  index,
  create,
  update,
  destroy,
};