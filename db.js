const Sequelize = require('sequelize');

const directorModel = require('./models/director');
const genreModel = require('./models/genre');
const memberModel = require('./models/member');
const movieModel = require('./models/movie');
const copyModel = require('./models/copy');
const bookingModel = require('./models/booking');
const actorModel = require('./models/actor');

const sequelize = new Sequelize('vc', 'root', 'zaq12wsx', {
    host: 'localhost',
    dialect: 'mysql'
});

const Director = directorModel(sequelize, Sequelize);
const Actor = actorModel(sequelize, Sequelize);
const Movie = movieModel(sequelize, Sequelize);
const Booking = bookingModel(sequelize, Sequelize);
const Copy = copyModel(sequelize, Sequelize);
const Genre = genreModel(sequelize, Sequelize);
const Member = memberModel(sequelize, Sequelize);

Director.hasMany(Movie, { as: 'movie', foreignKey: 'director_id' });
Genre.hasMany(Movie, { as: 'movie', foreignKey: 'genre_id' });
Movie.hasMany(Copy, { as: 'copy', foreignKey: 'movie_id' });
Copy.hasMany(Booking, { as: 'booking', foreignKey: 'copy_id' });
Member.hasMany(Booking, { as: 'booking', foreignKey: 'member_id' });
Movie.belongsToMany(Actor, {through: 'movies_actors', foreignKey: 'movie_id'});
Actor.belongsToMany(Movie, {through: 'movies_actors', foreignKey: 'actor_id'});

sequelize.sync({
    force: true
}).then(()=>{
    console.log("DB create");
});

module.exports = {
    Director,
    Genre, 
    Actor, 
    Member,
    Movie,
    Copy, 
    Booking
}