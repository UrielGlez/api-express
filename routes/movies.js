const express = require('express');
const controller = require('../controllers/movies');

const router = express.Router();

//CRUD = modelo, objeto o registro

router.post('/', controller.create);

router.get('/', controller.list);

router.get('/:id', controller.index);

router.put('/:id', controller.update);

router.delete('/:id', controller.destroy);

module.exports = router;